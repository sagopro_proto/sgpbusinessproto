PROTOC_FILE_MAIN = ./main.proto
PROTOC_BUSINESS = ./business/business.proto
PROTOC_BRAND_CAT = ./brand_category/brand_category.proto
PROTOC_BRAND = ./brand/brand.proto
PROTOC_BRAND_CV = ./brand_cover/brand_cover.proto
PROTOC_BUSINESS_MODEL = ./business_model/business_model.proto
PROTOC_BUSINESS_COVER = ./business_cover/business_cover.proto
PROTOC = protoc
PROTOC_FLAGS = -I . --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative
SWAG = -I . --openapiv2_out . 
GIT_ALL = git add . && git commit -m "--" && git push origin main

git:
	@echo "git all"
	-$(GIT_ALL)


bscv:
	@echo "create proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_BUSINESS_COVER)
	@echo "finishing proto..."

bm:
	@echo "create proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_BUSINESS_MODEL)
	@echo "finishing proto..."

bcv:
	@echo "create proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_BRAND_CV)
	@echo "finishing proto..."

br:
	@echo "create proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_BRAND)
	@echo "finishing proto..."

bc:
	@echo "create proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_BRAND_CAT)
	@echo "finishing proto..."

bs:
	@echo "create proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_BUSINESS)
	@echo "finishing proto..."

main:
	@echo "create proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_FILE_MAIN)
	@echo "finishing proto..."
	
all: pag bscv bm bcv bc br bs main

.PHONY: pag bscv bm bcv br bc bs main all git 
